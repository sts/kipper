/*
 *  Copyright 2015 CERN
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
*/

if (typeof Kipper == "undefined") Kipper = {};


Kipper.getAssertion = function() {
	return $.get("kipper/php/ssoGetAssertion.php", function(data) {
        	console.log("getting assertion");
	});
}

Kipper.getCredentials =  function(request,stsAddress) {
         return $.ajax({
                url: "/sts",
                type: "POST",
                contentType: "text/xml; charset=utf-8",
                headers: {SOAPAction: stsAddress},
                data: request,
                success : function(data2, status) {
			
                        var err = Kipper.ssoErrorString(data2);
                        if(err || data2 == null) {
                                console.log(err);
				console.log("Error contacting STS to request credendials");
                                return null;
                        }
                },
                error : function(jqXHR, textStatus, errorThrown) {
                        console.log("Error contacting STS to request credendials");
			return null;
                        }
                });
}
