/*
 *  Copyright 2015 CERN
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
*/

if (typeof Kipper == "undefined") Kipper = {};

Kipper.getConfig = function (){
        $.get("kipper/conf/config.xml", function(data){
            $(data).find('config').each(function() {
                sessionStorage.stsAddress=$(this).find('stsAddress').text();
                sessionStorage.ssoLogout=$(this).find('ssoLogout').text();
                sessionStorage.ssoLogin=$(this).find('ssoLogin').text();
		sessionStorage.stsVO=$(this).find('stsVO').text();
                });
        });
}

Kipper.getCertString = function (proxy,cert){
        if (sessionStorage.userProxy)
                return "-----BEGIN CERTIFICATE-----\r\n" + proxy.match(/.{1,64}/g).join("\r\n") + "\r\n-----END CERTIFICATE-----\r\n" +
                        "-----BEGIN CERTIFICATE-----\r\n" + cert.match(/.{1,64}/g).join("\r\n") + "\r\n-----END CERTIFICATE-----\r\n";
}

Kipper.getKeyString =function (key){
        return KEYUTIL.getPEM(KEYUTIL.getKeyFromPlainPrivatePKCS8Hex(b64tohex(key)), "PKCS1PRV");
}

//store proxy in the browser persisten local root via Filesystem API, in Chrome is avaialble via  filesystem:https://<webappurl>/persistent/proxy
Kipper.storeProxy = function(proxyString) {
	var bb = new Blob([proxyString], {type:'text/plain'});
	//wariting file
	navigator.webkitPersistentStorage.requestQuota(1024*1024, function(grantedBytes) {
	 window.webkitRequestFileSystem(window.PERSISTENT, 1024 * 1024, function(fs) {
	 	fs.root.getFile('proxy', {create: true}, function(fileEntry) { // fs.root is a DirectoryEntry object.
                	fileEntry.createWriter(function(writer) { // writer is a FileWriter object.
				writer.write(bb);
                                });
                        });
                });
        });
}


