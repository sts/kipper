## Kipper

The Kipper software enables acces via Federated Identity to X509 authentication based services

It's based on several components:

* CERN’s Single Sign On Portal (WebSSO)
* Shibboleth/Mellon, Federated Identity systems
* STS Server, token translation service to convert SAML2 tokens into X509 certificates
* VOMS, authorisation database managing access rights within Virtual Organisations
* IOTA-CA, Online Certificate Authority releasing Short lived X509 certificates  

### Contact
fts-devel <fts-devel@cern.ch>

### Details

The Kipper client software combines some tools and utilities to extend a Web Application to:

- Enable login via CERN WebSSO (which includes login via Edugain)
- Retrieve the Identity Assertion and make it available to the JS context.
- Ask the STS server for a Proxy certificate for a specific VO

#### JS 

##### Needed  includes

The kipper lib and thrid party libraries have to be included in your project
```
<script src="kipper/js/lib/jsrsasign-latest-all-min.js"></script>
<script src="kipper/js/lib/asn11.js"></script>
<script src="kipper/js/lib/base64E.js"></script>
<script src="kipper/js/lib/jquery.min.js"></script>
<script src="kipper/js/ssoHelper.js"></script>
<script src="kipper/js/certHelper.js"></script>
<script src="kipper/js/kipper.js"></script>
```

##### Login via CERN WebSSO

Your Web Application has to be registered to use the WebSSO with type SAML2 ( from https://sso-management.web.cern.ch) and in particular the Unverified External access has to be enabled
in order to allow access from the Edugain Federation users.

A specfic STS server instance for your WebApp has to be deployed then

When everything is setup and configured, you should be able to allow access via CERN WebSSO to your app, for instance by adding a Login link to your app, for instance

```
<a id='ssologin' href='" . /Shibboleth.sso/S2Login?target=/ . "'><span> LogIn </span></a>";
```

you can also retrieve and assertion's details and display the username + dynamically add logout link as shown in the  [file](php/ssoAuth.php).


##### Retrieve SAML2 Assertion

The SAML2 Assertion  associated with the Login via WebSSO can be retrieved via Kipper  and  its validity checked as follows

```
var jqxhr  = Kipper.getAssertion();

jqxhr.complete(function(data){
    if (Kipper.ssoAssertionTimeLeft(data.responseXML) < 60) {
    //The assertion is more than 1 minute old, the user should login again to get a new Assertion
    alert("The session has expired please Logout and Login again from SSO")
    return
    }
                
    // Let's check if we really got an assertion
    var err = Kipper.ssoErrorString(data.responseXML);
	if(err) 
    {
     alert("Error getting the assertion from SSO)   		

	}
```


##### Ask for a VOMS Proxy Certificate

In order to ask for a Proxy certitficate we need to first create a RSA Keypair.

The public Key of the Keypair, the SAML Assertion and the VOMS FQAN for which we want to ask for a proxy are needed in order to create the request for the STS service, as follows

```
var kp = KEYUTIL.generateKeypair("RSA", 2048);
        		
var req = Kipper.ssoSoapReq(<Assertion>, <sts Address>, 
hextob64(KEYUTIL.getHexFromPEM(KEYUTIL.getPEM(kp["pubKeyObj"]))),<VOMS FQAN>);
```

once the request is generated, we can contact the STS server in order to ask for a VOMS proxy

```
var jqxhr  = Kipper.getCredentials(<SOAP req>, <sts Address>);

jqxhr.complete(function(creds){
	...
	var proxy = Kipper.ssoGetProxy(creds.responseXML);
    var cert  = Kipper.ssoGetCertificate(creds.responseXML);
    //the key is extracted from the Key Pair object created before.
    var key   = hextob64(KEYUTIL.getHexFromPEM(KEYUTIL.getPEM(kp["prvKeyObj"], "PKCS8PRV")));
    ..
}		
```


### Detailed Configuration

#### Integration with SSO and Federated Identity

You will need either Shibboleth or Mellon configured on your WebApp server. Your server must also be registered in SSO with SAML2 profile [@CERN] (https://sso-management.web.cern.ch/sso-management/SSO/RegisterApplication.aspx).
 * Shibboleth
   * Follow the instructions on [this page](http://linux.web.cern.ch/linux/scientific6/docs/shibboleth.shtml) but
   * Use the following [shibboleth2.xml](https://svnweb.cern.ch/trac/fts3/raw-attachment/wiki/WEBFTS/shibboleth2.xml) instead
   * Save SP metadata from https://your-webapp-host/Shibboleth.sso/Metadata
 * Mellon
   * Install lasso and mod_auth_mellon_cern
   * Generate SP keys and metadata with /usr/libexec/mod_auth_mellon/mellon_create_metadata.sh
   * Configure proper file paths in two .htaccess files in root and /sso folders of your WebAPP

You must put your SP metadata somewhere on your server and manually send a link to SSO support. This is mandatory. You will also need one of the Federation metadata files from [this page] (https://espace.cern.ch/authentication/CERN%20Authentication/Configure%20a%20SAML2%20Application.aspx)

In case of JS integration a client's browser will send AJAX requests to STS (another domain), which is not CORS-aware. In order to make it simple you need to configure reverse-proxy mapping of STS into your WebApp namespace with the following additional lines in <VirtualHost> section of your webapp.conf for Apache:
```
SSLProxyEngine on
ProxyPass /sts https://your-sts-host:8443/sts/wstrust
ProxyPassReverse /sts https://your-sts-host:8443/sts/wstrust
```