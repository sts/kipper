import lxml.etree as ET
from datetime import datetime
import requests
from textwrap import TextWrapper
import sys

class Kipper:
  """
  """
  def __init__(self,vo, stsAddress):
    self.vo = vo
    self.stsAddress = stsAddress

  ## In the case of  a django application, authentication information is 
  ## included in the incoming request.
  ## Extract SAML token
  def getAssertion(self):
    #check if Mellon or Shibboleth
    if ('MELLON_SAML_RESPONSE' in request.META):
      return request.META.get('MELLON_SAML_RESPONSE').decode('base64')   
    elif ('Shib-Assertion-01' in request.META):
      return  request.META.get('Shib-Assertion-01').decode('base64')
    else:
      raise Exception('No SAML Token')


  def makeSTSRequest(self, stsAddress,vo,saml):
    samlxml = ET.fromstring(saml)

    ## Get the Assertion out of the token
    assertion = samlxml.find('{urn:oasis:names:tc:SAML:2.0:assertion}Assertion')
    ## Extract the user, for use in debugging
    user = assertion.find(".//{urn:oasis:names:tc:SAML:2.0:assertion}Attribute[@Name='http://schemas.xmlsoap.org/claims/CommonName']/{urn:oasis:names:tc:SAML:2.0:assertion}AttributeValue").text
    ## Extract the token ID
    assertion_id = assertion.get('ID')
    ## Extract the expiration date
    conditions = assertion.find('{urn:oasis:names:tc:SAML:2.0:assertion}Conditions')
    expiration = conditions.get('NotOnOrAfter')
    ## Check Token Validity
    expiration_date = datetime.strptime(expiration,'%Y-%m-%dT%H:%M:%S.%fZ')
    current_date = datetime.now()
    if ( expiration_date < current_date ):
        raise Exception('Token expired')

    ## Create SOAP Request for STS
    soap_body = """<?xml version="1.0" encoding="UTF-8"?>
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <soap:Header xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <wsa:Action xmlns:wsa="http://www.w3.org/2005/08/addressing">
    http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wsa:Action>
    <wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">
    urn:uuid:99999999-0000-0000-0000-000000000000</wsa:MessageID>
    <wsa:To xmlns:wsa="http://www.w3.org/2005/08/addressing">
    """+stsAddress+"""</wsa:To>
    <sbf:Framework version="2.0" xmlns:sbf="urn:liberty:sb"/>
    <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
    <wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
    <wsu:Created xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
    """+current_date.strftime('%Y-%m-%dT%H:%M:%S.%fZ')+"""
    </wsu:Created>
    </wsu:Timestamp>
    """+ET.tostring(assertion)+"""
    </wsse:Security>
    </soap:Header>
    <soap:Body xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
    <wst:RequestSecurityToken Context="urn:uuid:00000000-0000-0000-0000-000000000000" xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
    <wst:RequestType xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">http://docs.oasis-open.org/ws-sx/ws-trust/200512/RST/Issue</wst:RequestType>
    <wst:TokenType xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">urn:glite.org:sts:GridProxy</wst:TokenType>
    <wst:Claims Dialect="http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0" xmlns:wst="http://docs.oasis-open.org/ws-sx/ws-trust/200512">
    <wsse:SecurityTokenReference xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
    <wsse:Reference URI="#"""+assertion_id+"""" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"/>
    </wsse:SecurityTokenReference>
    </wst:Claims>
    <gridProxy:GridProxyRequest xmlns:gridProxy="urn:glite.org:sts:proxy" lifetime="86400">
    <gridProxy:VomsAttributeCertificates xmlns:gridProxy="urn:glite.org:sts:proxy">
    <gridProxy:FQAN xmlns:gridProxy="urn:glite.org:sts:proxy">"""+vo+""":/"""+vo+"""</gridProxy:FQAN>
    </gridProxy:VomsAttributeCertificates>
    </gridProxy:GridProxyRequest>
    </wst:RequestSecurityToken>
    </soap:Body>
    </soap:Envelope>"""

    ## Specify the incoming data format and SOAPAction (required) as sts endpoint
    soap_headers = {
        'Content-Type': 'text/xml; charset=utf-8',
        'SOAPAction' : stsAddress
    }

    ## Send SOAP Request
    return requests.post(stsAddress,data=soap_body,headers=soap_headers, verify=False)
  
  def extractProxy(self,stsResponse):

    ## Check SOAP Response & extract elements
    sts_response = ET.fromstring(stsResponse.content)
    voms_proxy = ''
    certificate  = ''

    for token in sts_response.findall('.//{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd}BinarySecurityToken'):
        tokenid = token.get('{http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd}Id')
        if tokenid == '#X509SecurityToken':
            certificate = token.text
        elif tokenid == '#VOMSSecurityToken':
            voms_proxy = token.text
        else :
            raise Exception('Unexpected security tokens in STS response')

    key = sts_response.find('.//{http://docs.oasis-open.org/ws-sx/ws-trust/200512}BinarySecret')
    if not len(voms_proxy) or not len(certificate) or key is None :
        raise Exception('Unable to generate certificate')

    textWrapper = TextWrapper(width=64)
    proxy_string = textWrapper.fill(voms_proxy.replace("\n",""))
    proxy_string="-----BEGIN CERTIFICATE-----\r\n"+proxy_string+"\r\n-----END CERTIFICATE-----\r\n-----BEGIN PRIVATE KEY-----\r\n"
    key.text = textWrapper.fill(key.text)
    proxy_string=proxy_string+key.text+"\r\n-----END PRIVATE KEY-----\r\n-----BEGIN CERTIFICATE-----\r\n"
    cert= textWrapper.fill(certificate.replace("\n",""))
    proxy_string=proxy_string+cert+"\r\n-----END CERTIFICATE-----\r\n";

    return proxy_string


  def getProxy(self):
    '''
    '''
    #first get the assertion
    proxy =""
    try:
      assertion = getAssertion()
      response = makeSTSRequest(self.vo, self.stsAddress,assertion)
      proxy = extractProxy(response)
    except Exception, e:
      print e
    return proxy
